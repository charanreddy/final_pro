#CMake version
cmake_minimum_required (VERSION 2.6)


#including directories


include_directories ("include")



#adding libraries

add_library (testbench "src/test_bench.cpp" "src/memory")



#adding executables

add_executable (test_main test/test_main.cpp)

#linking libraries


target_link_libraries (test_main testbench systemc)

#enable test


enable_testing ()

add_test (TestMem test_main)
