#include <systemc.h>
#include "test_bench.hpp"


void TestBench::init_values_memory() {
	unsigned i;
	//cout << "depth: " << DEPTH << "\n";
	mem_chip_sel.write(1);
	mem_write_en.write(1);
	for (i=0; i<DEPTH; i++) {
		mem_din.write(i);
		mem_add.write(i);
		wait(1,SC_NS);
	}
	

	mem_chip_sel.write(1);
	mem_otp_en.write(1);
	for (unsigned i=0;i < DEPTH; i++) {
	  mem_add.write(i);
		wait(1,SC_NS);
		save_memory_out_vector[i] = mem_dout.read().to_uint();
	}

		
}


int TestBench::check_memory() {
	int error=0;
	
  for (unsigned i=0; i<DEPTH;i++) {
		if (save_memory_out_vector[i] != i) error |= 1;
	}
	return error;
}
