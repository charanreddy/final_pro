#include <systemc.h>
#include "memory.hpp"

void Memory::entry() {
  while(true) {
  	wait();
  	if (chip_sel->read() == 1 && otp_en->read() == 1) {
			//cout << "read_memory: " << add->read().to_uint() << "\n";
			dout->write(data[add->read().to_uint()]);  
		}
		else if (chip_sel->read() == 1 && write_en->read() == 1) {
			//cout << "write_memory: " << add->read().to_uint() << "\n";
			data[add->read().to_uint()] = din->read();
		}
	}

}
