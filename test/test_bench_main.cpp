#include <systemc.h>
#include "testbench.hpp"

int sc_main(int argc, char* argv[]) {

	TestBench testbench("testbench");

	sc_start(1000, SC_NS);

  return testbench.check_memory();
}
