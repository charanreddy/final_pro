#ifndef TESTBENCH_HPP
#define TESTBENCH_HPP

#include "memory.hpp"

SC_MODULE(TestBench) {
	
	
	//valiables for memory
	static const unsigned ADDR_WIDTH = 8;
	static const unsigned DATA_WIDTH = 8;
	static const unsigned DEPTH = 1 << ADDR_WIDTH;

	//variables for test memory
	unsigned save_memory_out_vector[DEPTH]; 
	

	//ports for memory
	sc_signal<sc_lv<ADDR_WIDTH> > mem_add;
	sc_signal<sc_lv<DATA_WIDTH> >mem_din;
	sc_signal<sc_lv<DATA_WIDTH> >mem_dout;
	sc_signal<bool> mem_chip_sel; //chip select
	sc_signal<bool> mem_write_en; //write enable
	sc_signal<bool> mem_otp_en; //output enable

	Memory mem1;
	
	
	SC_CTOR(TestBench) : mem1("memory1") {
	
		//memory
		SC_THREAD(init_values_memory);
		mem1.add(mem_add);
		mem1.din(mem_din);
		mem1.dout(mem_dout);
		mem1.chip_sel(mem_chip_sel);
		mem1.write_en(mem_write_en);
		mem1.otp_en(mem_otp_en);
  } 

	//memory
	void init_values_memory();
  
  public: 
	int check_memory();
};


#endif
