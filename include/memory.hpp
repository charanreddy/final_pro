#ifndef MEMORY_H_
#define MEMORY_H_

SC_MODULE(Memory) {
	static const unsigned ADDR_WIDTH = 8;
	static const unsigned DATA_WIDTH = 8;
	static const unsigned DEPTH = 1 << ADDR_WIDTH;
	
	sc_in<sc_lv<ADDR_WIDTH> > add;
	sc_in<sc_lv<DATA_WIDTH> >din;
	sc_out<sc_lv<DATA_WIDTH> >dout;
	sc_in<bool> chip_sel; //chip select
	sc_in<bool> write_en; //write enable
	sc_in<bool> otp_en; //output enable
	
	sc_lv<DATA_WIDTH> data[DEPTH];
	
	SC_CTOR(Memory) {
		SC_THREAD(entry);
		sensitive << chip_sel << write_en << otp_en << add << din;
	
	}
	
	private:
	void entry();
};

#endif /*end of memory_H_*/
